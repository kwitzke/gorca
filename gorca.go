package main

import (
	"github.com/gogearbox/gearbox"
	"gitlab.com/kwitzke/gorca/gorcabackend"
)

func main() {
	// Setup gearbox
	g := gearbox.New()

	// Define your handlers
	g.Get("/", func(ctx gearbox.Context) {
		ctx.SendString(gorcabackend.GetQuote())
	})

	// Start service
	g.Start(":3000")
}
